module.exports = {
    addNewUser: async (req, res) => {
        UsersService.addUser(req.body.name)
        .then(() => {
            res.status(200).send({
                message: 'User Created',
            });
        })
        .catch((err) => {
            res.status(400).send({
                message: err.message,
            });
        })
    },
    getUsers: async (req, res) => {
        UsersService.getAllUsers()
        .then((users) => {
            res.status(200).send({
                data: users,
            });
        })
        .catch((err) => {
            res.status(400).send({
                message: err.message,
            })
        })
    },
}