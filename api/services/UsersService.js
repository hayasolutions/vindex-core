module.exports = {
    addUser(name) {
        return new Promise((resolve, reject) => {
            Users.create({
                name: name,
            }, (err) => {
               if (err) {
                   return reject(err);
               }
               resolve();
            });
        });
    },
    getAllUsers() {
        return new Promise((resolve, reject) => {
            Users.find({}, (err, users) => {
                if(err) {
                    return reject(err);
                }
                return resolve(users);
            });
        });
    }
}