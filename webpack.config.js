/* eslint-disable */
var path = require('path');
/* eslint-enable */

module.exports = {
    devtool: 'inline-source-map',
    entry: [
        './views/server/index.js',
    ],
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                loader: 'babel-loader',
                include: path.join(__dirname, 'views/server'),
                exclude: /node_modules/,
                query: {
                    presets: [
                        'es2015',
                        'react',
                    ],
                },
            },
        ]
    },
    resolve: {
        extensions: [
            '.js',
            '.jsx',
        ],
    },
    stats: 'normal'
}