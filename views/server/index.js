import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import createStore from './redux';

import App from './app';

const JSX = (
  /* eslint-disable no-undef */
  <Provider store={createStore(window.REDUX_STATE)}>
    <Router>
      <App />
    </Router>
  </Provider>
);

const app = document.getElementById('root');

ReactDOM.hydrate(JSX, app);
