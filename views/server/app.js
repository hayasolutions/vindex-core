import React from 'react';

import { connect } from 'react-redux';

import PropTypes from 'prop-types';

import { setNote, getNote } from './redux';

class App extends React.Component {
  componentDidMount() {
    const { setNote } = this.props; // eslint-disable-line no-shadow
    setNote('Test Note');
  }

  render() {
    const { note } = this.props;
    return (
      <div>
        { note }
      </div>
    );
  }
}

App.propTypes = {
  setNote: PropTypes.func.isRequired,
  note: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  return {
    note: getNote(state),
  };
}

const mapDispatchToProps = {
  setNote,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
