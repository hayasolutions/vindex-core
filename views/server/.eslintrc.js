module.exports = {
    extends: [
        'airbnb',
    ],
    rules: {
        sourceType: 'module',
        'react/jsx-filename-extension': [
            1,
            {
                extensions: [
                    '.js',
                    '.jsx',
                ], 
            },
        ]
    }
}