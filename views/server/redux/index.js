import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import notesReducer from './reducers/notesReducer';
import setNote, {
  getNote,
} from './actions/notesActions';

export default initialState => createStore(
  combineReducers({
    note: notesReducer,
  }),
  initialState,
  applyMiddleware(thunk),
);

export {
  setNote,
  getNote,
};
