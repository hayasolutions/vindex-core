import {
  SET_NOTE,
} from '../reducers/notesReducer';

export function getNote(state) {
  return state.note;
}

export default function setNote(note) {
  return {
    type: SET_NOTE,
    payload: {
      note,
    },
  };
}
