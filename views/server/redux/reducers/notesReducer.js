export const SET_NOTE = 'vindex.action.SET_NOTE';

export default (state = '', { type, payload }) => {
  switch (type) {
    case SET_NOTE:
      return payload.note;
    default:
      return state;
  }
};
