module.exports = () => {
    return new Promise((resolve, reject) => {
      try {
        vindex.log.info('Loading Policies');
        glob(`${appRoot}/api/policies/**/*.js`, (err, files) => {
          if (err) {
            return reject(err);
          }
          async.forEachOf(files, (file, fileKey, filesCb) => {
            const fileName = path.basename(file, '.js');
            if (/\w+Policy/.test(fileName)) {
              const jsFile = require(path.resolve(file));
              if (typeof jsFile === 'function' &&
                  getFunctionArguments(jsFile).length === 3) {
                  return filesCb(null, global[fileName] = jsFile, vindex.log.info(`Policy ${fileName} Loaded`));
              } else {
                  return filesCb(`Wrong Policy Format: ${fileName}`);
              }
            } else {
              return filesCb(`Wrong Filename Format: ${fileName}`);
            }
          }, (filesErr) => {
            if (filesErr) {
              return reject(filesErr);
            }
            return resolve();
          })
        });
      } catch (promiseError) {
        return reject(promiseError);
      }
    });
  };