module.exports = () => {
    return new Promise((resolve, reject) => {
      vindex.log.info('Loading Helpers');
      glob(`${appRoot}/api/helpers/**/*.js`, (err, files) => {
        if (err) {
          return reject(err);
        }
        let helpersObj = {};
        async.forEachOf(files, (file, fileKey, filesCb) => {
          const fileName = path.basename(file, '.js');
          if (/\w+Helper/.test(fileName)) {
            return filesCb(null, helpersObj[fileName] = require(path.resolve(file)),
              vindex.log.info(`Helper ${fileName} Loaded`));
          } else {
            return filesCb(`Wrong Filename Format: ${fileName}`);
          }
        }, (filesErr) => {
          if (filesErr) {
            return reject(filesErr);
          }
          return resolve(_.extend(global.vindex, {
              helpers: helpersObj,
          }));
        })
      });
    })
  };