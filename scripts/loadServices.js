module.exports = () => {
    return new Promise((resolve, reject) => {
      try {
        vindex.log.info('Loading Services');
        glob(`${appRoot}/api/services/**/*.js`, (err, files) => {
          if (err) {
            return reject(err);
          }
          async.forEachOf(files, (file, fileKey, filesCb) => {
            const fileName = path.basename(file, '.js');
            if (/\w+Service/.test(fileName)) {
              return filesCb(null, global[fileName] = require(path.resolve(file)),
              vindex.log.info(`Service ${fileName} Loaded`));
            } else {
              return filesCb(`Wrong Filename Format: ${fileName}`);
            }
          }, (filesErr) => {
            if (filesErr) {
              return reject(filesErr);
            }
            return resolve();
          })
        });
      } catch (promiseError) {
        return reject(promiseError);
      }
    })
  };