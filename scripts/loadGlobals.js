const globals = require(require('path').resolve(require('get-root-path').rootPath, './config/globals'));

module.exports = async () => {
    return new Promise((resolve, reject) => {
        globals.async.forEachOf(globals, (requiredModule, moduleKey, cb) => {
            return cb(null, global[moduleKey] = requiredModule);
        }, (err) => {
            if (err) {
                return reject(err);        
            }
            return resolve();
        })
    })
}