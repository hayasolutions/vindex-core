const express = require('express');
const http = require('http');
const https = require('https');
const cors = require('cors');
const session = require('express-session');

/**
 * Application Instance
 */
const app = express();

module.exports = () => {
    return new Promise(async (resolve, reject) => {
        try {
            /**
             * Server Settings
             */
            const httpSettings = require(path.resolve(appRoot, './config/http'));
            const httpsSettings = require(path.resolve(appRoot, './config/https'));
            const sessionSettings = require(path.resolve(appRoot, './config/session'));
            const middlewaresSettings = require(path.resolve(appRoot, './config/middlewares'));
            const policiesSettings = require(path.resolve(appRoot, './config/policies'));
            const appSettings = require(path.resolve(appRoot, './config/app'));

            if (appSettings.apiRoute 
                && (appSettings.apiRoute === '' 
                || appSettings.apiRoute === '/'
                || appSettings.apiRoute === '/assets'
                || appSettings.apiRoute === '/input/log')) {
                    reject(new Error(`Cannot Use Reserved API Route ${appSettings.apiRoute}`));
                }

            /**
             * ReactJS Settings
             */
            esmRequire = require('esm')(module);
            require('babel-register');
            const createStore = esmRequire(path.resolve(appRoot, './views/server/redux')).default;
            const hyper = require('react-hyperscript');
            const { StaticRouter } = require('react-router-dom');
            const { Provider } = require('react-redux');
            const reactDOMServer = require('react-dom/server');
            const AppComponent = require(path.resolve(appRoot, './views/server/app')).default;
            const handlebars = require('handlebars');

            /**
             * Configure CORS
             */
            app.use(cors(require(path.resolve(appRoot, './config/cors'))));

            /**
             * Load Middlewares
             */
            if(_.isArray(middlewaresSettings.middlewaresOrder) && !_.isEmpty(middlewaresSettings.middlewaresOrder)){
                const omittedMiddlewares = _.omit(middlewaresSettings, 'middlewaresOrder');
                for (let i = 0; i < middlewaresSettings.middlewaresOrder.length; i++) {
                    await app.use(omittedMiddlewares[middlewaresSettings.middlewaresOrder[i]]);
                }
            }

            /**
             * Load Session Storage
             */
            if (sessionSettings.store) {
                app.use(session((() => {
                    const omittedSettings = _.omit(sessionSettings, 'store');
                    if (sessionSettings.store.type) {
                        omittedSettings.store = new (require(sessionSettings.store.type)(session))(sessionSettings.store.config)
                    }
                    return omittedSettings;
                })()))
            } else {
                app.use(session(sessionSettings));
            }

            /**
             * Serve Assets
             */
            app.use('/assets', express.static(path.join(appRoot, 'assets')));
            vindex.log.info('Route /assets Exposed');

            /**
             * Serve Interceptor
             */
            if (appSettings.interceptor.enabled) {
                app.use(require('access-watch-express-logger')('websocket', `ws://localhost:${appSettings.interceptor.port}/input/log`));
                vindex.log.info('Route /input/log Exposed For Interceptor');
            }
            
            /**
             * Serve ReactJS
             */
            app.use(express.static(path.resolve(appRoot, './build')));
            app.use((req, res, next) => {
                res.renderReactView = function(location, initialStates) {
                    fs.readFile(path.resolve(appRoot, './views/client/index.hbs'), 'utf8', (err, template) => {
                        if(err) {
                        return res.status(400).send({
                            message: err.message,
                        });
                        }
                        const store = createStore(initialStates);
                        const compiledTemplate = handlebars.compile(template);
                        res.writeHead(200, {
                            "Content-Type": "text/html"
                        });
                        res.end(compiledTemplate({
                            htmlData: reactDOMServer.renderToString(hyper(Provider, {
                                store: store,
                            }, [
                                hyper(StaticRouter, {
                                    context: {},
                                    location: location,
                                }, [
                                    hyper(AppComponent),
                                ])
                            ])),
                            storeData: JSON.stringify(store.getState()),
                        }));
                    })
                };
                next();
            });
            
            /**
             * Load Routes
             */
            async.forEachOf(require(path.resolve(appRoot, './config/routes')), (routeConfig, routeKey, cb) => {
                const controllerPath = routeConfig.function.split('.');
                const functionPath = (() => {
                    let functionObj = global[controllerPath[0]];
                    for (let i = 1; i < controllerPath.length; ++i){
                        functionObj = functionObj[controllerPath[i]];
                    }
                    return functionObj;
                })();
                if (policiesSettings[controllerPath[0]]) {
                    if (_.isArray(policiesSettings[controllerPath[0]])) {
                        app[routeConfig.method](`${!routeConfig.isView ? appSettings.apiRoute : ''}${routeConfig.route}`, policiesSettings[controllerPath[0]], functionPath);
                        vindex.log.info(`Route ${!routeConfig.isView ? appSettings.apiRoute : ''}${routeConfig.route} Exposed, Function ${routeConfig.function}`);
                    } else if (_.isPlainObject(policiesSettings[controllerPath[0]])){
                        app[routeConfig.method](`${!routeConfig.isView ? appSettings.apiRoute : ''}${routeConfig.route}`, (() => {
                            let functionObj = policiesSettings[controllerPath[0]];
                            for (let i = 1; i < controllerPath.length; ++i){
                                functionObj = functionObj[controllerPath[i]];
                            }
                            return functionObj;
                        })(), functionPath);
                        vindex.log.info(`Route ${!routeConfig.isView ? appSettings.apiRoute : ''}${routeConfig.route} Exposed, Function ${routeConfig.function}`);
                    }
                } else {
                    app[routeConfig.method](`${!routeConfig.isView ? appSettings.apiRoute : ''}${routeConfig.route}`, functionPath);
                    vindex.log.info(`Route ${!routeConfig.isView ? appSettings.apiRoute : ''}${routeConfig.route} Exposed, Function ${routeConfig.function}`);
                }
                
                cb();
            }, (err) => {
                if (err) {
                    return reject(new Error(err));
                }
                if (httpsSettings.ssl && !_.isEmpty(Object.keys(httpsSettings.ssl))) {
                    const { key, ca, cert } = httpsSettings.ssl;
                    if (key && ca && cert) {
                        https.createServer(httpsSettings.ssl, app).listen(httpsSettings.port);
                        vindex.log.info(`HTTPS Server Started At Port ${httpsSettings.port}`);
                    }
                }
                http.createServer(app).listen(httpSettings.port);
                resolve(vindex.log.info(`HTTP Server Started At Port ${httpSettings.port}`));
            });
        } catch (promiseError) {
            return reject(promiseError);
        }
    });
}