module.exports = () => {
    return new Promise((resolve, reject) => {
        if (!require(path.resolve(appRoot, './config/app')).interceptor) {
            return resolve();
        }
        const fileData = `
            const accessWatch = require('..')();

            const { pipeline, input } = accessWatch;
            
            const webSocketServerInput = input.websocket.create({
            name: 'Vindex',
            type: 'server',
            path: '/input/log'
            })
            
            pipeline.registerInput(webSocketServerInput)
        `;
        fs.exists(path.resolve(appRoot, './node_modules/access-watch/config/config.js'), (exists) => {
            if (!exists) {
                vindex.log.info('Configuring Interceptor');
                fs.writeFile(path.resolve(appRoot, './node_modules/access-watch/config/config.js'), fileData, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                });
            } else {
                return resolve();
            }
        });
    });
}