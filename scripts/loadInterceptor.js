module.exports = () => {
    return new Promise((resolve, reject) => {
        try {
            const { enabled, port } = require(path.resolve(appRoot, './config/app')).interceptor;
            if (!enabled) {
                return resolve();
            }
            vindex.log.info('Loading Interceptor');
            const accessWatchDir = path.resolve(appRoot, './node_modules/access-watch/')
            const instance = require('child_process').spawn('node', [
                `start.js`,
                `config/config.js`,
            ], {
                cwd: accessWatchDir,
                env: {
                    PORT: port,
                }
            });
            instance.stdout.on('data', data => {
                if (data.toString('utf8').trim() === `HTTP and Websocket Server listening on port ${port}`) {
                    resolve(vindex.log.info(`Interceptor Server Started At Port ${port}`));
                }
            })
            instance.on('error', (err) => {
                reject(err);
            })
        } catch (promiseError) {
            return reject(promiseError);
        }
    });
}