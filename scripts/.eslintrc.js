module.exports = {
    extends: [
        'eslint:recommended',
        'plugin:node/recommended',
    ],
    rules: {
        'no-undef': [
            'off',
        ],
    },
}