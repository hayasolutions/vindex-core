module.exports = () => {
    const appSettings = require(path.resolve(appRoot, './config/app'));
    if (!appSettings.logo) {
        return(vindex.log.info('Vindex Framework'));
    }
    const logo = `
               @@                            @@@                               
               @                             @@@                               
@@         @@ @@@  @@  @@@@@@          @@@@@@@@@      @@@@@@@@     @@@      @@@
@@         @@ @@@  @@@@@    @@@     @@@@     @@@   @@@@     @@@@    @@@   @@@  
 @@       @@  @@@  @@@        @@   @@@       @@@  @@@         @@@    @@@ @@@   
  @@     @@   @@@  @@         @@  @@@        @@@ @@@  @@@@@@@@@@@@     @@@    
   @@   @@    @@@  @@         @@  @@@        @@@ @@@                   @@@    
    @@ @@     @@@  @@         @@   @@@       @@@  @@@          @     @@@ @@@   
     @@@      @@@  @@         @@    @@@      @@@   @@@@     @@@@    @@@   @@@  
      @       @@@  @@         @@       @@@@@@@@@      @@@@@@@@     @@@     @@@
                                                                    `;
    
    return new Promise((resolve) => {
        const splittedArr = require('split-lines')(logo);
        async.forEachOf(splittedArr, (line, index, cb) => {
            if (line.trim()) {
                vindex.log.info(line);
            }
            cb();
        }, () => {
           resolve(vindex.log.info('')); 
        });
    })
    
}