module.exports = () => {
    const PrettyError = require('pretty-error');
    const peInstance = new PrettyError();
    peInstance.appendStyle({
        'pretty-error > header > title > kind': {
            display: 'none'
        },
        'pretty-error > header > colon': {
        
            display: 'none'
        },
        'pretty-error > header > message': {
            padding: '0 0'
        },
    });
    _.extend(global.vindex, {
        log: winston.createLogger(require(path.resolve(appRoot, './config/log'))),
    })
    vindex.log._error = vindex.log.error;
    vindex.log.error = (...args) => {
        if(typeof args[0] === Error) {
            return vindex.log._error(peInstance.render(...args));
        }
        return vindex.log._error(...args);
    }
}