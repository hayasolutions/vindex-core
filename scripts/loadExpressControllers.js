module.exports = () => {
  return new Promise((resolve, reject) => {
    try {
      vindex.log.info('Loading Controllers');
      glob(`${appRoot}/api/controllers/**/*.js`, (err, files) => {
        if (err) {
          return reject(err);
        }
        if (_.isEmpty(files)) {
          return reject('No Controllers Were Found');
        }
        async.forEachOf(files, (file, fileKey, filesCb) => {
          const fileName = path.basename(file, '.js');
          if (/\w+Controller/.test(fileName)) {
            const jsFile = require(path.resolve(file));
            if (Object.keys(jsFile)) {
              async.forEachOf(jsFile, (controllerFunction, functionKey, cb) => {
                if (typeof controllerFunction === 'function' &&
                  getFunctionArguments(controllerFunction).length === 2) {
                  return cb();
                }
                return cb(true);
              }, (err) => {
                if (err) {
                  return filesCb(`Invalid Controller ${fileName}`);
                }
                return filesCb(null, global[fileName] = jsFile, vindex.log.info(`Controller ${fileName} Loaded`));
              });
            }
          } else {
            return filesCb(`Wrong Filename Format: ${fileName}`);
          }
        }, (filesErr) => {
          if (filesErr) {
            return reject(filesErr);
          }
          return resolve();
        })
      });
    } catch (promiseError) {
      return reject(promiseError);
    }
  })
};