module.exports = () => {
    return new Promise((resolve, reject) => {
        try {
            let databaseSettings = require(path.resolve(appRoot, './config/databases'));
            let defaultDB = '';
            const dbConnections = {};
            if (databaseSettings.default) {
                defaultDB = databaseSettings.default;
                databaseSettings = _.omit(databaseSettings, 'default');
            }
            async.forEachOf(databaseSettings, (database, name, cb) => {
                if (defaultDB && name === defaultDB) {
                    dbConnections['default'] = mongoose.createConnection(mongoUri.format(database.uri), _.extend(database.config, {
                        useNewUrlParser: true
                    }), (err) => {
                        if (err) {
                            return cb(err);
                        }
                        cb(null, vindex.log.info(`Connected to Database ${database.uri.database}`));
                    });
                } else {
                    dbConnections[name] = mongoose.createConnection(mongoUri.format(database.uri), _.extend(database.config, {
                        useNewUrlParser: true
                    }), (err) => {
                        if (err) {
                            return cb(err);
                        }
                        cb(null, vindex.log.info(`Connected to Database ${database.uri.database}`));
                    });
                }
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve(_.extend(global.vindex, {
                    dbConnections: dbConnections,
                }));
            });
        } catch (promiseError) {
            return reject(promiseError);
        }
    });
}