module.exports = (promise) => {
    return promise.then((data) => [data]).catch(err => {
        throw err;
    });
};