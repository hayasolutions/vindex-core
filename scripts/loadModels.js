module.exports = () => {
    return new Promise((resolve, reject) => {
        try {
            glob(`${appRoot}/api/models/**/*.js`, (err, files) => {
                if (err) {
                    return reject(err);
                }
                if (_.isEmpty(files)) {
                    return vindex.log.info('No Models Were Found');
                }
                
                vindex.log.info('Loading Models');
    
                const loadedModels = {};
                const databaseSettings = require(path.resolve(appRoot, './config/databases'));
                const mongoosePlugins = require(path.resolve(appRoot, './config/mongoosePlugins'));
    
                async.forEachOf(files, (file, fileKey, filesCb) => {
                    const fileName = path.basename(file, '.js');
                    if (pluralize.isSingular(fileName)) {
                        return filesCb(`File ${fileName} Must Be Plural`);
                    }
                    global[fileName] = {};
                    loadedModels[fileName] = file;
                    return filesCb(null);
                }, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    async.forEachOf(loadedModels, (modelPath, modelName, modelsCb) => {
                        let { database, attributes, config, methods, static, query } = require(path.resolve(appRoot, modelPath));
                        let schema = new mongoose.Schema(attributes, _.extend(config, {
                            timestamps: {},
                        }));
                        _.extend(schema.methods, methods);
                        _.extend(schema.statics, static);
                        _.extend(schema.query, query);
                        schema.plugin(require('mongoose-archive'));
                        schema.plugin(require('mongoose-deep-populate')(mongoose));
                        async.forEachOf(mongoosePlugins, (plugin, name, pluginsCb) => {
                            schema.plugin(plugin);
                            return pluginsCb();
                        }, () => {
                            if(database && database !== databaseSettings.default) {
                                global[modelName] = vindex.dbConnections[database].model(modelName, schema);
                            } else {
                                global[modelName] = vindex.dbConnections['default'].model(modelName, schema);
                            }
                            return modelsCb(null, vindex.log.info(`Model ${modelName} Loaded`));
                        });
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve();
                    });
                });
            });
        } catch (promiseError) {
            return reject(promiseError);
        }
    });
}