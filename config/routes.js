module.exports = [
    {
        method: 'get',
        route: '/',
        function: 'NotesController.getNotes',
        isView: true,
    },
    {
        method: 'post',
        route: '/users',
        function: 'UsersController.addNewUser',
    },
    {
        method: 'get',
        route: '/users',
        function: 'UsersController.getUsers',
    }
]