module.exports = {
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.prettyPrint(),
        winston.format(function(info) {
            prefix = `${moment().format('YYYY-MM-DD hh:mm:ss').trim()}`
            info.message = `[${prefix}] ${info.message}`;
            return info;
        })()
    ),
    transports: [
        new winston.transports.Console({
            format: winston.format.simple()
        }),
    ]
}