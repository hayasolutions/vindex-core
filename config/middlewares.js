module.exports = {
    middlewaresOrder: [
        'cookieParser',
        'skipper',
    ],

    skipper: require('skipper')(),
    cookieParser: require('cookie-parser')(),
}