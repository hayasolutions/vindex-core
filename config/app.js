module.exports = {
    apiRoute: '/api',
    interceptor: {
        enabled: true,
        port: 8080,
    },
    logo: true,
}