module.exports = {
    mongooseHidden: require('mongoose-hidden')({
        defaultHidden: {
            __v: true,
        }
    }),
}