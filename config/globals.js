module.exports = {
    path: require('path'),
    _: require('lodash'),
    axios: require('axios'),
    fs: require('fs'),
    glob: require('glob'),
    getFunctionArguments: require('get-function-arguments'),
    async: require('async'),
    winston: require('winston'),
    moment: require('moment'),
    pluralize: require('pluralize'),
    mongoUri: require('mongodb-uri'),
    mongoose: require('mongoose'),
}