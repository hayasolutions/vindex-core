/**
 * Vindex Bootstrap
 */
const {
    configureLogger,
    configureExpress,
    configureInterceptor,
    loadExpressControllers,
    loadGlobals,
    loadExpressPolicies,
    loadHelpers,
    loadServices,
    loadDatabases,
    loadInterceptor,
    loadBranding,
    loadModels,
    utils,
} = require('./scripts');

const { promiseCatch } = utils;

module.exports = {
    loadApp: async () => {
        try {
            await promiseCatch(loadGlobals());
            configureLogger();
            await promiseCatch(loadBranding());
            await promiseCatch(configureInterceptor());
            await promiseCatch(loadInterceptor());
            await promiseCatch(loadDatabases());
            await promiseCatch(loadHelpers());
            await promiseCatch(loadModels());
            await promiseCatch(loadServices());
            await promiseCatch(loadExpressPolicies());
            await promiseCatch(loadExpressControllers());
            await promiseCatch(configureExpress());
        } catch (err) {
            vindex.log.error(err);
        }
    },
};